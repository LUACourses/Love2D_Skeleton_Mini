-- Entité représentant un sprite
-- Utilise une forme d'héritage simple: sprite qui hérite de class
-- ****************************************
SPRITE_SCALE = 2
ANIMATION_SPEED = 1 / 8

-- Type de sprites
SPRITE_TYPE = {
  HUMAN = "STHuman",
  MONSTER = "STMonster"
}

-- États possibles pour les sprites
SPRITE_STATUS= {
  NORMAL = "SSNormal",
  DEAD = "SSDead"
}

--- Crée un pseudo objet de type sprite
-- Note: certaines valeurs sont divisées par l'échelle de l'écran
-- le sprite est ajouté à la variable pList
function newSprite (pList, pvX, pvY, pType, pImageList, pNFrames)
  ---- debugFunctionEnter("newSprite ", pImage, pX, pY )
  --assertEqualQuit(viewport, nil, "newSprite:viewport", true)
  if (pType == nil) then pType = SPRITE_STATUS.NORMAL end

  local lSprite = class:new()
  lSprite.lisIndex = 0
  lSprite.type = pType
  lSprite.images = {}
  lSprite.currentFrame = 1
  lSprite.maxSpeed = (pvX + pvY) / 2
  lSprite.speed = lSprite.maxSpeed
  lSprite.vX = pvX or 1
  lSprite.vY = pvY or 1
  lSprite.animationTimer = ANIMATION_SPEED
  lSprite.life = 1

  local index
  for index = 1, pNFrames do
    local filename = "assets/images/"..pImageList..tostring(index)..".png"
    lSprite.images[index] = love.graphics.newImage(filename)
  end -- for

  lSprite.x = 0
  lSprite.y = 0

  lSprite.width = lSprite.images[1]:getWidth()
  lSprite.height = lSprite.images[1]:getHeight()

  function lSprite.update (pDt)
      -- mise à jour de l'animation du sprite
    lSprite.animationTimer = lSprite.animationTimer - pDt
    if (lSprite.animationTimer <= 0) then
      lSprite.animationTimer = ANIMATION_SPEED
      lSprite.currentFrame = lSprite.currentFrame + 1
      if (lSprite.currentFrame > #lSprite.images) then lSprite.currentFrame = 1 end
    end
  end

  function lSprite.draw ()
    if (lSprite.status ~= SPRITE_STATUS.DEAD) then
      local frame = lSprite.images[lSprite.currentFrame]
      local w = lSprite.width
      local h = lSprite.height
      love.graphics.draw(frame, lSprite.x - w / 2, lSprite.y - h / 2)
    end
  end

   -- collisions avec les bords de l'écran
  function lSprite.collideWithBorder()
    local isCollison = false
    local offset = 2
    local xMin = lSprite.width / 2
    local yMin = lSprite.height / 2
    local xMax = screenWidth - lSprite.width / 2
    local yMax = screenHeight - lSprite.height / 2
    if (lSprite.x < xMin) then
      lSprite.x = xMin + offset
      isCollison = true
    elseif (lSprite.x > xMax) then
      lSprite.x = xMax - offset
      isCollison = true
    end
    if (lSprite.y < yMin) then
      lSprite.y = yMin + offset
      isCollison = true
    elseif (lSprite.y > yMax) then
      lSprite.y = yMax - offset
      isCollison = true
    end
    return isCollison
  end

  function lSprite.destroy ()
  end

  function lSprite.loseLife (pValue)
    if (lSprite.status == SPRITE_STATUS.DEAD) then return end
    if (pValue == nil) then pValue = 1 end
    lSprite.life = lSprite.life - pValue
    if (lSprite.life < 0) then
      lSprite.destroy()
    end
  end

  function lSprite.drawByType ()
    -- chaque type de sprite implémente cette fonction
  end

  function lSprite.updateByType (pDt, pList)
    -- chaque type de sprite implémente cette fonction
  end

  -- Ajout du sprite à la liste
  if (pList ~= nil) then
    -- index du sprite dans la table (utile pour sa suppression)
    lSprite.listIndex = #pList + 1
    table.insert(pList, lSprite)
  end

  return lSprite, lSprite.listIndex
end -- newSprite
