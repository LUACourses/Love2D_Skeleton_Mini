-- Regroupe les constantes nécessaires au framework
-- Elles ne devraient pas être modifiées pendant l'éxécution
-- ****************************************

-- configuration du DEBUG
-- ******************************
-- Définit des niveau d'affichage des information de debug (affiche des infos supplémentaires)
-- 0: aucun
-- 1: messages affichés via la fonction debugMessage
-- 2: comme précédent mais affiche aussi les infos de debug dans le HUD (variable debugInfos)
-- 3: comme précédent mais affiche aussi les identifiants des sprites
-- 4: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnter (SANS ceux qui peuvent remplir le log)
-- 5: comme précédent mais affiche aussi les messages affichés via la fonction debugFunctionEnterNL (AVEC ceux qui peuvent remplir le log)
DEBUG_MODE = 4
-- position X de la fenêtre du jeu en mode DEBUG
DEBUG_WINDOWS_X = 100
-- position Y de la fenêtre du jeu en mode DEBUG
DEBUG_WINDOWS_Y = 100
-- écran sur lequel est positionné la fenêtre du jeu en mode DEBUG
DEBUG_DISPLAY_NUM = 2

-- Répertoires utilisés
-- ******************************
-- dossier contenant les fichiers du framework
FOLDER_BASE = "base"
-- dossier contenant les fichiers du jeu
FOLDER_GAME = "game"

-- AFFICHAGE
-- ******************************
-- différents écrans du jeu
SCREEN_PLAY  = "screenPlay"
SCREEN_PAUSE = "screenPause"
SCREEN_START = "screenStart"
SCREEN_END   = "screenEnd"
SCREEN_NEXT  = "screenNext"

-- Facteur d'échelle pour l'affichage en X
SCREEN_SCALE_X = 1

-- Facteur d'échelle pour l'affichage en Y
SCREEN_SCALE_Y  = SCREEN_SCALE_X

-- positions relatives utilisées par la fonction displayText()
POS_TOP_LEFT      = "posTopLeft"
POS_TOP_RIGHT     = "posTopRight"
POS_TOP_CENTER    = "posTopCenter"
POS_CENTER        = "posCenter"
POS_CENTER_LEFT   = "posCenterLeft"
POS_CENTER_RIGHT  = "posCenterRight"
POS_BOTTOM_LEFT   = "posBottomLeft"
POS_BOTTOM_RIGHT  = "posBottomRight"
POS_BOTTOM_CENTER = "posBottomCenter"