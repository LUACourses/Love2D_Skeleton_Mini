-- Entité représentant l'état du jeu à un instant donné
-- ****************************************

--- Crée un pseudo objet de type appState
-- @return un pseudo objet appState
function newAppState ()
  local lState = {}

  --- Initialise l'objet
  -- @param pLevelToWin (OPTIONNEL) niveau à atteindre pour gagner le jeu
  -- @param pPointForLevel (OPTIONNEL) nombre de points à marquer pour passer un niveau
  function lState.initialize (pLevelToWin, pPointForLevel)
    debugFunctionEnter("appState.initialize ", pLevelToWin, pPointForLevel)
    if (pLevelToWin == nil) then pLevelToWin = 1 end
    if (pPointForLevel == nil) then pPointForLevel = 10 end

    -- État courant du jeu
    lState.currentScreen = SCREEN_START
    --lState.currentScreen = SCREEN_PLAY -- DEBUG uniquement
    -- le jeu a t-elle démarrée ?
    lState.hasStarted = false
    -- nombre de niveaux à atteindre pour gagner le jeu
    lState.levelToWin = pLevelToWin
    -- nombre de points à marquer pour passer un niveau
    lState.pointForLevel = pPointForLevel
  end -- appState.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  -- initialisation par défaut
  lState.initialize()
  return lState
end -- newAppState