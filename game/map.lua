-- Entité représentant une map
-- Utilise une forme d'héritage simple: map qui hérite de class
-- ****************************************

CELL_WIDTH = 34
CELL_HEIGHT = 13
MAP_WIDTH = 6
MAP_HEIGHT = 9
MAP_SPACING = 5

CELL_CLOSE = "cell_close"
CELL_OPEN = "cell_open"

--- Crée un pseudo objet de type map
-- @return un pseudo objet map
function newMap ()
  ---- debugFunctionEnter("newMap ")

  -- création d'un objet parent
  local lMap = class:new()

  lMap.width = MAP_WIDTH
  lMap.height = MAP_HEIGHT
  lMap.grid = {}
  lMap.x = 5
  lMap.y = 5
  lMap.cellWidth = CELL_WIDTH
  lMap.cellHeight = CELL_HEIGHT

  function lMap.initialize()
    debugFunctionEnter("lMap.initialize")
    local r, c
    -- initialise la carte avec des cellules vides
    for r = 1, lMap.width do
      lMap.grid[r] = {}
      for c = 1, lMap.height do
        lMap.grid[r][c] = lMap.createCell(r, c)
      end
    end
  end

  function lMap.draw()
    ---- debugFunctionEnterNL("lMap.draw")
    local r, c
    local posX, posY
    local color
    for r = 1, lMap.width do
      for c = 1, lMap.height do
        posX = lMap.x + (lMap.cellWidth + MAP_SPACING) * (c - 1)
        posY = lMap.y + (lMap.cellHeight + MAP_SPACING) * (r - 1)
        if (lMap.grid[r][c].status == CELL_OPEN) then
          color = {128, 128, 128, 255}
        else
          color = {255, 255, 255, 255}
        end
        love.graphics.setColor(color)
        love.graphics.rectangle ("fill", posX, posY, lMap.cellWidth, lMap.cellHeight)
      end
    end
    color = {255, 255, 255, 255}
  end

  function lMap.createCell(pRow, pCol)
    local room = {}
    room.row = pRow
    room.col = pCol
    room.status = CELL_CLOSE
    return room
  end

  lMap.initialize()

  return lMap
 end -- newMap
