-- Entité représentant un joueur
-- Utilise une forme d'héritage simple: joueur qui hérite de sprite
-- ****************************************

--- Crée un pseudo objet de type joueur
-- le joueur est ajouté à la variable pList
function newPlayer (pList, pEntities)
  ---- debugFunctionEnter("newPlayer")
  local lPlayer = newSprite(pList, 0, 0, SPRITE_TYPE.HUMAN, "player_", 4)
  lPlayer.life = 100
  lPlayer.score = 0
  lPlayer.level = 1
  lPlayer.maxSpeed = 83
  lPlayer.speed = 100
  lPlayer.rotation = 0
  lPlayer.hasWon = false
  lPlayer.friction = 10

  lPlayer.useFrictionAndRotation = false

  function lPlayer.destroy ()
    lPlayer.status = SPRITE_STATUS.DEAD
  end

  local spriteLoseLife = lPlayer.loseLife
  function lPlayer.loseLife ()
    lPlayer.vX = lPlayer.vX * 0.8
    lPlayer.vY = lPlayer.vY * 0.8
    lPlayer.speed = lPlayer.speed * 0.8
    lPlayer.x = lPlayer.x + math.random (-5, 5)
    lPlayer.y = lPlayer.y + math.random (-5, 5)

    spriteLoseLife()
  end

  function lPlayer.updateByType (pDt)
    if (lPlayer.status == SPRITE_STATUS.DEAD) then
      endScreenCountDowwn = endScreenCountDowwn - pDt
      return
    end
    local oldX = lPlayer.x
    local oldY = lPlayer.y

    if (lPlayer.useFrictionAndRotation) then
      -- déplacement du joueur en utilisant la rotation, la vitesse et la friction (comme un vaisseau spatial)
      local speedStep = 1
      local rotationStep = 0.05

      lPlayer.score = lPlayer.score + pDt

      if (love.keyboard.isDown(settings.playerKeys.moveLeft)) then
         lPlayer.rotation =  lPlayer.rotation + rotationStep
      end
      if (love.keyboard.isDown(settings.playerKeys.moveRight)) then
          lPlayer.rotation =  lPlayer.rotation - rotationStep
      end
      if (love.keyboard.isDown(settings.playerKeys.moveUp) and lPlayer.speed < lPlayer.maxSpeed - speedStep) then
        lPlayer.speed =  lPlayer.speed + speedStep
      end
      if (love.keyboard.isDown(settings.playerKeys.moveDown) and lPlayer.speed > - lPlayer.maxSpeed + speedStep) then
        lPlayer.speed =  lPlayer.speed - speedStep
      end

      lPlayer.speed = lPlayer.speed - lPlayer.friction * pDt

      local speedFactor = lPlayer.speed * pDt
      lPlayer.vX = math.cos(lPlayer.rotation) * speedFactor
      lPlayer.vY = math.sin(lPlayer.rotation) * speedFactor

      lPlayer.y = lPlayer.y + lPlayer.vY
      lPlayer.x = lPlayer.x - lPlayer.vX
    else
      -- déplacement du joueur en utilisant uniquement la vitesse

      local speedFactor = lPlayer.speed * pDt

      if (love.keyboard.isDown(settings.playerKeys.moveLeft)) then
         lPlayer.x =  lPlayer.x - speedFactor
      end
      if (love.keyboard.isDown(settings.playerKeys.moveRight)) then
         lPlayer.x =  lPlayer.x + speedFactor
      end
      if (love.keyboard.isDown(settings.playerKeys.moveUp)) then
         lPlayer.y =  lPlayer.y - speedFactor
      end
      if (love.keyboard.isDown(settings.playerKeys.moveDown)) then
         lPlayer.y =  lPlayer.y + speedFactor
      end
    end


    -- collisions avec les bords de l'écran
    if (lPlayer.collideWithBorder()) then
      lPlayer.x = oldX
      lPlayer.y = oldY
    end
  end

  function lPlayer.drawByType ()
    local color = {200, 200, 255, 255}
    if (lPlayer.status ~= SPRITE_STATUS.DEAD) then
      -- on affiche la vie du joueur
      content = "LIFE:"..tostring(math.floor(lPlayer.life))
      displayText(content, fontNormal, POS_TOP_LEFT, 0, 0, color)
      -- on affiche le score du joueur
      content = "SCORE:"..tostring(math.floor(lPlayer.score))
      displayText(content, fontNormal, POS_TOP_RIGHT, 0, 0, color)
    else
      content = "YOU ARE DEAD !"
      displayText(content, fontNormal, POS_CENTER, 0, 0, color)
      local w = lPlayer.width
      local h = lPlayer.height
      love.graphics.draw(imgDead, lPlayer.x - w / 2, lPlayer.y - h / 2)
      if (endScreenCountDowwn < 0) then loseGame() end
    end
  end

  return lPlayer
end -- newPlayer
