-- Entité représentant un pnj
-- Utilise une forme d'héritage simple: pnj qui hérite de sprite
-- ****************************************

-- États possibles pour les pnjs
ZSTATES = {
  NONE = "ZSnone",
  WALK = "ZSwalk",
  ATTACK = "ZSattack",
  DAMAGE = "ZSdamage",
  PATROL = "ZSpatrol"
}

--- Crée un pseudo objet de type pnj
-- le pnj est ajouté à la variable pList
function newPnj (pList)
  ---- debugFunctionEnter("newPnj")
  local lPnj = newSprite(pList, 0, 0, SPRITE_TYPE.MONSTER, "monster_", 2)

  function lPnj.initialize ()
    lPnj.x = math.random(10, screenWidth - lPnj.width - 10)
    lPnj.y = math.random(10, (screenHeight / 2) - lPnj.height - 10)
    lPnj.status = ZSTATES.NONE
    lPnj.maxSpeed = math.random(5, 50) / 100
    lPnj.detectRange = math.random(20, 150)
    lPnj.damageRange = 6
    lPnj.damages = 0.1
    lPnj.speed = lPnj.maxSpeed
    lPnj.attackSpeed = lPnj.maxSpeed * 3 -- la vitesse d'attaque est le triple de la vitesse de base
  end

  function lPnj.updateByType (pDt, pEntities)

    if (lPnj.status == SPRITE_STATUS.DEAD) then return end

    -- déplacements du pnj en fonction de sa vitesse
    lPnj.x = lPnj.x + lPnj.vX * pDt
    lPnj.y = lPnj.y + lPnj.vY * pDt

    if (lPnj.status == nil) then
      DebugMessage("lPnj.updateByType:lPnj.status est indéfini")
    else
      -- collisions avec les bords de l'écran
      if (lPnj.collideWithBorder()) then
        lPnj.status = ZSTATES.PATROL
      end

      -- machine à état
      -- *****************
      if (lPnj.status == ZSTATES.NONE) then
        -- AUCUNE ACTION EN COURS
        -- **************************
        lPnj.status = ZSTATES.PATROL

      elseif (lPnj.status == ZSTATES.WALK) then
        -- MARCHE vers la cible définie
        -- **************************
        lPnj.speed = lPnj.maxSpeed

        -- analyse de l'environnement
        -- on analyse les sprites à l'écran
        local index, sprite
        for index, sprite in ipairs(pEntities) do
          local distance = math.dist(lPnj.x, lPnj.y, sprite.x, sprite.y)
          -- est ce que le sprite est un humain ?
          if (sprite.type == SPRITE_TYPE.HUMAN and sprite.status ~= SPRITE_STATUS.DEAD) then
            -- est-il dans la portée de détection du pnj ?
            if (distance < lPnj.detectRange) then
              -- oui, le sprite cible l'humain
              lPnj.target = sprite
              lPnj.status = ZSTATES.ATTACK
            end

            -- est ce qu'un pnj en mode attaque est dans la portée de détection du pnj ?
          elseif (sprite.type == SPRITE_TYPE.ZOMBIE and sprite ~= lPnj and sprite.status == ZSTATES.DAMAGE) then
            if (distance < lPnj.detectRange) then
              -- oui, on se déplace vers lui
              lPnj.target = sprite
              lPnj.speed = lPnj.maxSpeed / 2
              lPnj.status = ZSTATES.WALK
            end
          end --if (sprite.type == SPRITE_TYPE.HUMAN) then
        end -- for

      elseif (lPnj.status == ZSTATES.ATTACK) then
        local distance = math.dist(lPnj.x, lPnj.y, lPnj.target.x, lPnj.target.y)

        -- ATTAQUE LA CIBLE
        -- **************************
        if (lPnj.target == nil and lPnj.target.status ~= SPRITE_STATUS.DEAD) then
          -- pas de cible, on recommence à patrouiller
          lPnj.status = ZSTATES.PATROL
          -- est ce que la cible dans la portée de morsure du pnj ?
        elseif (distance < lPnj.damageRange and lPnj.target.type == SPRITE_TYPE.HUMAN and lPnj.target.status ~= SPRITE_STATUS.DEAD) then
          -- oui, le sprite mord la cible
          lPnj.speed = 0
          lPnj.status = ZSTATES.DAMAGE
          -- est ce que la cible (humaine) est dans encore la portée de détection du pnj ?
        elseif (distance < lPnj.detectRange and lPnj.target.type == SPRITE_TYPE.HUMAN and lPnj.target.status ~= SPRITE_STATUS.DEAD) then
          -- oui, on se dirige vers elle en accélérant
          lPnj.speed = lPnj.attackSpeed
          lPnj.moveToTarget()
        else
          -- non, on recommence à patrouiller
          lPnj.speed = lPnj.maxSpeed
          lPnj.status = ZSTATES.PATROL
        end

      elseif (lPnj.status == ZSTATES.PATROL) then
        -- PATROUILLE
        -- **************************
        -- on choisi une cible au hasard dans l'écran
        lPnj.target = {
          x = math.random(0, screenWidth),
          y = math.random(0, screenHeight)
        }
        lPnj.moveToTarget()
        lPnj.status = ZSTATES.WALK

      elseif (lPnj.status == ZSTATES.DAMAGE) then
        -- MORD LA CIBLE
        -- **************************
        if (math.dist(lPnj.x, lPnj.y, lPnj.target.x, lPnj.target.y) < lPnj.damageRange and lPnj.target.status ~= SPRITE_STATUS.DEAD) then
          if (lPnj.target.life ~= nil) then
            lPnj.target.loseLife (lPnj.damages)
          end
        else
          lPnj.status = ZSTATES.ATTACK
        end
      end -- if (lPnj.status == ZSTATES.NONE) then
    end
  end

  function lPnj.moveToTarget ()
    -- DEPLACEMENT EN DIRECTION DE LA CIBLE
    -- **************************
    -- on ajoute un peu d'imprécision sur la cible à atteindre. Ca donne l'impression que le pnj "farfouille" autour de la cible une fois atteinte
    local destX = lPnj.target.x + math.random(-10, 10)
    local destY = lPnj.target.y + math.random(-10, 10)
    local angle = math.angle(lPnj.x, lPnj.y, destX, destY)
    lPnj.vX = lPnj.speed * 60 * math.cos(angle)
    lPnj.vY = lPnj.speed * 60 * math.sin(angle)
  end

  function lPnj.drawByType ()
    -- on affiche une image indiquant que le pnj attaque
    local w = lPnj.width
    local h = lPnj.height
    if (lPnj.status == ZSTATES.ATTACK) then
      love.graphics.draw(imgAlert, lPnj.x - imgAlert:getWidth() / 2, lPnj.y - h / 2 - imgAlert:getHeight() - 2)
    end
    -- on affiche l'état du sprite
    if (debugFlagEnabled) then
      love.graphics.print(lPnj.listIndex .. ":" .. lPnj.status, lPnj.x - 10, lPnj.y - h - 10)
    end
  end

  return lPnj
end -- newPnj
