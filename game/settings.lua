-- Entité regroupant les paramètres du jeu
-- ****************************************

function newSettings ()
  local lSettings = {}

  --- Initialise l'objet
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en X
  -- @param pSensibilityX (OPTIONNEL) coefficient de sensibilité de la souris en Y
  -- @param pFriction (OPTIONNEL) coefficient de friction du mouvement. Mettre à 0 pour ne pas appliquer
  -- @param pGravity (OPTIONNEL) coefficient de gravité. Mettre à 0 pour ne pas appliquer
  function lSettings.initialize (pSensibilityX, pSensibilityY, pFriction, pGravity)
    debugFunctionEnter("settings.initialize")
    if (pSensibilityX == nil) then pSensibilityX = 1 end
    if (pSensibilityY == nil) then pSensibilityY = 1 end

    -- mapping clavier pour les touches globales à le jeu
    lSettings.appKeys = {
      nextPower     = "kp*",
      nextLevel     = "kp+",
      loseLive      = "kp-",
      loseGame      = "end",
      winGame       = "home",
      quitGame      = "escape",
      debugSwitch   = "f9",
      moveWithKeys  = "f10",
      moveWithMouse = "f11",
      grabMouse     = "f12",
      pauseGame     = "p",
      startGame     = "r",
      previousMusic = "f1",
      nextMusic     = "f2"
    }

    -- mapping clavier pour les touches pouvant être modifiées par chaque joueur
    lSettings.playerKeys = {
      action1      = "space",
      action2      = "lctrl",
      -- touches de déplacement
      moveUp       = "up",
      moveDown     = "down",
      moveLeft     = "left",
      moveRight    = "right",
      -- touches de déplacement alternatives
      moveUpAlt    = "z",
      moveDownAlt  = "s",
      moveLeftAlt  = "q",
      moveRightAlt = "d"
    }

    -- souris
    lSettings.mouse = {
      sensibilityX = pSensibilityX,
      sensibilityY = pSensibilityY
    }
  end

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  -- initialisation par défaut
  lSettings.initialize()
  return lSettings
end --newSettings
