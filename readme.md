## Love2D Skeleton Mini

### Description
Une version light du projet Love2D_Skeleton qui ne contient que les éléments de bases pour une application love2D.
Il contient 1 prototype de jeu qui propose une IA simplifiée basée sur une machine à états.

Aucune documentation n'est fournie pour l'instant.
Toutefois, pour faciliter la prise en main du framework, le code est clair et très largement  commenté (en français uniquement pour le moment).
===
### Objectifs
Aucun.

### Mouvements et actions du joueur
Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.
Mettre en pause: touche P.
Relancer la partie: touche R.
Quitter le jeu: clic sur la croix ou touche escape.

#### en mode debug uniquement
Confiner la souris dans la fenêtre (ON/OFF): touche F12.
Activer le mode Debug en live (ON/OFF): touche F9.
Générer un nouveau donjon: touche F8.

Prochain Niveau: touche + (pavé numérique).
Perdre la partie: touche Fin.
Gagner la partie: touche Debut.

### Interactions
- Pour le joueur:
  - collisions avec les pnj.
  - collisions avec les bords de la zone de jeu.
- Pour les pnjs:
  - collisions avec le joueur.
  - collisions avec les bords de la zone de jeu.

### Copyrights
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
A light version of the Love2D_Skeleton project that only contains the basics for a love2D application.
It contains 1 game prototype that uses a simplified AI based on a state machine.

No documentation is provided at this time.
However, to facilitate the handling of the framework, the code is clear and widely commented (French only for the moment).

===
### Goals
Nothing special.

### Movements and actions of the player
Move the player: moves the mouse.
Move the player: ZQSD keys or the arrows on the keyboard.
Action (fire): left click
Pause: P key.
Restart the game: R key.
Exit the game: click on the cross or escape key.

### debug mode only
Confining the mouse in the window (ON/OFF): F12 key.
Activate the Live Debug mode (ON/OFF): F9 key.
Next Level: + key (keypad).
Lose the game: End key.
Win the game: Home key.

### Interactions
- For the player:
   - collisions with the NPCs
   - collisions with edges of the game area.
- For the NPCs:
   - collisions with the player
   - collisions with edges of the game area.

### Copyrights
Written in LUA and with the Love2D Framework.
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)