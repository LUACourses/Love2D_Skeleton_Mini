-- Écran de fin
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées
-- ****************************************

--- Actualise l'écran de fin
-- @param pDt delta time
function updateEndScreen (pDt)
  ---- debugFunctionEnterNL("updateEndScreen") -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end -- updateEndScreen

--- Affiche l'écran de fin
function drawEndScreen ()
  ---- debugFunctionEnterNL("drawEndScreen") -- ATTENTION cet appel peut remplir le log
  love.graphics.push()
  love.graphics.scale(SPRITE_SCALE, SPRITE_SCALE)

  local content
  local h = fontNormal:getHeight("|")
  local color = {200, 200, 255, 255}

  player.score = math.floor(player.score)

  if (player.hasWon) then
    content = "Bravo,partie gagnee !"
    color = {0, 200, 0, 255} -- couleur vert sans transparence
  else
    content = "GAME OVER :o("
    color = {200, 0, 0, 255} -- couleur rouge sans transparence
  end
  displayText(content, fontTitle, POS_CENTER, 0, 0, {color})

  color = {255, 160, 0, 255}
  displayText('Appuyer sur "'..settings.appKeys.quitGame ..'" pour quitter le jeu', fontNormal, POS_CENTER, 0, h * 3, color) -- couleur orange sans transparence
  displayText('Appuyer sur "'..settings.appKeys.startGame..'" pour rejouer', fontNormal, POS_CENTER, 0 , h * 4, color)
  displayText("Votre score est de "..player.score, fontNormal, POS_CENTER, 0, h * 7, color)

  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.pop()
end -- drawEndScreen

--- Gestion du "keypressed" sur l'écran de fin
-- @param dynamiques
function keypressedEndScreen (pKey, pScancode, pIsrepeat)
  ---- debugFunctionEnterNL("keypressedEndScreen ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
  if (pKey == settings.appKeys.quitGame) then quitGame();return end
  if (pKey == settings.appKeys.startGame) then startGame();return end
end -- keypressedEndScreen

--- Gestion du "mousepressed" sur l'écran de fin
-- @param dynamiques
function mousepressedEndScreen (pX, pY, pButton, pIstouch)
  debugFunctionEnter("mousepressedEndScreen")
  -- rien à faire pour le moment
end -- mousepressedEndScreen

--- Gestion du "mousemoved" sur l'écran de fin
-- @param dynamiques
function mousemovedEndScreen (pX, pY, pDX, pDY, pIstouch)
  ---- debugFunctionEnterNL("mousemovedEndScreen ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end -- mousemovedEndScreen