-- Écran de jeu
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées
-- ****************************************

function updatePlayScreen (pDt)
  ---- debugFunctionEnterNL("updatePlayScreen ",pDt) -- ATTENTION cet appel peut remplir le log
  local index, sprite
  for index, sprite in ipairs(lstSprites) do

    -- mise à jour du sprite
    sprite.update(pDt)

    sprite.updateByType(pDt, lstSprites)

  end -- for
end -- updatePlayScreen

--- Affiche l'écran de jeu
function drawPlayScreen ()
  ---- debugFunctionEnterNL("drawPlayScreen") -- ATTENTION cet appel peut remplir le log
  love.graphics.push()
  love.graphics.scale(SPRITE_SCALE, SPRITE_SCALE)

  map.draw()

  local index
  for index, sprite in ipairs(lstSprites) do
    sprite.draw()

    sprite.drawByType()
  end -- for
  love.graphics.pop()
end -- drawPlayScreen

--- Gestion du "keypressed" sur l'écran de jeu
-- @param dynamiques
function keypressedPlayScreen (pKey, pScancode, pIsrepeat)
  ---- debugFunctionEnterNL("keypressedPlayScreen ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
  if (DEBUG_MODE > 0) then
    -- en mode debug uniquement, touches spéciales pour tweaker la partie
    if (pKey == settings.appKeys.nextLevel) then nextLevel(); return end
    if (pKey == settings.appKeys.loseGame) then loseGame(); return end
    if (pKey == settings.appKeys.winGame) then winGame(); return end
  end -- if (DEBUG_MODE > 0) then

  if (pKey == settings.appKeys.pauseGame) then pauseGame() end

end -- keypressedPlayScreen

--- Gestion du "mousepressed" sur l'écran de jeu
-- @param dynamiques
function mousepressedPlayScreen (pX, pY, pButton, pIstouch)
  debugFunctionEnter("mousepressedPlayScreen")
end -- mousepressedPlayScreen

--- Gestion du "mousemoved" sur l'écran de jeu
-- @param dynamiques
function mousemovedPlayScreen (pX, pY, pDX, pDY, pIstouch)
  ---- debugFunctionEnterNL("mousemovedPlayScreen ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log

end -- mousemovedPlayScreen