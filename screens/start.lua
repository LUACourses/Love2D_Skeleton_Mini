-- Écran de Démarrage
-- Ensemble des fonctions de mise a jour, d'affichage et de traitement des entrées
-- ****************************************

--- Actualise l'écran (menu) de départ
-- @param pDt delta time
function updateStartScreen (pDt)
  ---- debugFunctionEnterNL("updateStartScreen") -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
  debugInfos = ""
end -- updateStartScreen

--- Affiche l'écran (menu) de départ
function drawStartScreen ()
  ---- debugFunctionEnterNL("drawStartScreen") -- ATTENTION cet appel peut remplir le log
  love.graphics.push()
  love.graphics.scale(SPRITE_SCALE, SPRITE_SCALE)
  local content
  local color = {200, 200, 255, 255}
  local h = fontNormal:getHeight("|")

  content = love.window.getTitle()
  displayText(content, fontTitle, POS_CENTER, 0, 0, {255, 160, 0, 255})

  content = "Appuyer sur une touche ou cliquer pour lancer le jeu"
  displayText(content, fontNormal, POS_CENTER, 0, h * 2 , color)

  content = "Appuyer sur ESC pour quitter"
  displayText(content, fontNormal, POS_CENTER, 0, h * 3, color)

  content = "Utilisez les FLECHES pour vous deplacer"
  displayText(content, fontNormal, POS_CENTER, 0, h * 4, color)

  content = "Eviter tous vos ennemis et restez en vie"
  displayText(content, fontNormal, POS_CENTER, 0, h * 7, color)

  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.pop()

end -- drawStartScreen

--- Gestion du "keypressed" sur l'écran (menu) de départ
-- @param dynamiques
function keypressedStartScreen (pKey, pScancode, pIsrepeat)
  ---- debugFunctionEnterNL("keypressedStartScreen ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
  if (pKey ~= nil) then startGame() end
end -- keypressedStartScreen

--- Gestion du "mousepressed" sur l'écran (menu) de départ
-- @param dynamiques
function mousepressedStartScreen (pX, pY, pButton, pIstouch)
  debugFunctionEnter("mousepressedStartScreen")
  startGame()
end -- mousepressedStartScreen

--- Gestion du "mousemoved" sur l'écran (menu) de départ
-- @param dynamiques
function mousemovedStartScreen (pX, pY, pDX, pDY, pIstouch)
  ---- debugFunctionEnterNL("mousemovedStartScreen",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
  -- rien à faire pour le moment
end -- mousemovedStartScreen
